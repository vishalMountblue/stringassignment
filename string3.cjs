const pattern = (str) => {
  let regex = /\/(\d)(\d)\//g;

  let patternValue = str.match(regex);
  if (patternValue === null) {
    regex = /\/(\d)\//g;
    patternValue = str.match(regex);
  }
  return patternValue;
};

module.exports = pattern;
