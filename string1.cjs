const stringEdit = (str) => {
  
  let editedString = str.replace("$", "")
 
  return Number.isInteger(parseInt(editedString)) ? editedString : 0
};

//stringEdit();

module.exports = stringEdit
