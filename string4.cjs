const name = (obj) => {
  let fullName = "";
  if (obj.first_name) {

    
    fullName += obj.first_name + " ";
  }

  if (obj.middle_name) {
    fullName += obj.middle_name + " ";
  }

  if (obj.last_name) {
    fullName += obj.last_name;
  }

  fullName = fullName.toLowerCase().split(' ').map(function (word) {
    return word.charAt(0).toUpperCase() + word.slice(1)
  }).join(' ');
  
  return fullName
};

module.exports = name
