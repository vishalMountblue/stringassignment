const completeString = (arr) => {
  if (arr.length === 0) return [];

  return arr.reduce(
    (accumulator, currentValue) => accumulator + " " + currentValue
  );
};

module.exports = completeString
