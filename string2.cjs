const split = (str) => {
  let arr = [];

  arr = str.split(".");

  for (let k = 0; k < arr.length; k++) {

    //console.log(`num= ${arr[k]} type = ${typeof arr[k]}`);

    
    arr[k] = parseInt(arr[k]);

    if (!Number.isInteger(arr[k])) {
      return [];
    }
  }

  return arr;
};

module.exports = split;
